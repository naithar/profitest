//
//  LoadingTest.swift
//  profiTest
//
//  Created by Sergey Minakov on 07.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import XCTest

@testable import profiTest

class TestLoadingPresenter: LoadingInteractorPresenter, LoadingViewPresenter {

    var startCalled = false
    var listCalled = false
    var authCalled = false
    
    var view: LoadingPreseterView?
    var interactor: LoadingPresenterInteractor?
    
    init(view: LoadingPreseterView, interactor: LoadingPresenterInteractor) {
        self.view = view
        self.interactor = interactor
        
        self.view?.presenter = self
        self.interactor?.presenter = self
    }
    
    func start() {
        self.startCalled = true
        self.interactor?.checkAuthorization()
    }
    
    func showList() {
        self.listCalled = true
    }
    
    func showAuth() {
        self.authCalled = true
    }
}

class LoadingTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testLoading() {
        let window = UIWindow()
        
        LoadingWireframe.display(in: window)
        
        XCTAssertNotNil(window.rootViewController)
        XCTAssert(window.rootViewController is LoadingViewController)
    }
    
    func testLoadingWireframeGenericType() {
        let window = UIWindow()
        
        LoadingWireframe.display(in: window, with: TestService.self)
        
        XCTAssertNotNil(window.rootViewController)
        XCTAssert(window.rootViewController is LoadingViewController)
    }
    
    func testLoadingToAuth() {
        let view = LoadingViewController()
        let interactor = LoadingInteractor(type: TestService.self)
        let presenter = TestLoadingPresenter(view: view, interactor: interactor)
        
        view.viewDidAppear(true)
        
        XCTAssertTrue(presenter.startCalled)
        XCTAssertFalse(presenter.listCalled)
        XCTAssertTrue(presenter.authCalled)
    }
    
    func testLoadingToList() {
        let view = LoadingViewController()
        let interactor = LoadingInteractor(type: TestService.self)
        let presenter = TestLoadingPresenter(view: view, interactor: interactor)
        
        TestService.shared.authorize { _ in }
        
        view.viewDidAppear(true)
        
        XCTAssertTrue(presenter.startCalled)
        XCTAssertTrue(presenter.listCalled)
        XCTAssertFalse(presenter.authCalled)
    }
    
}
