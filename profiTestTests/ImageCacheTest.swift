//
//  ImageCacheTest.swift
//  profiTest
//
//  Created by Sergey Minakov on 07.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import XCTest

@testable import profiTest

class ImageCacheTest: XCTestCase {
    
    var cache = ImageCache()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCacheStorage() {
        self.cache["item"] = UIImage()
        
        XCTAssertNotNil(self.cache["item"])
    }
    
    func testCacheClearing() {
        self.testCacheStorage()
        
        self.cache.clear()
        
        XCTAssertNil(self.cache["item"])
    }
}
