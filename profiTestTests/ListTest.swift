//
//  ListTest.swift
//  profiTest
//
//  Created by Sergey Minakov on 07.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import XCTest

@testable import profiTest

class TestListPresenter: ListViewPresenter, ListInteractorPreseter {
    
    var loadCalled = false
    var selectCalled = false
    var setupCalled = false
    var updateCalled = false
    
    var array = [Friend]()
    
    var view: ListPresenterView?
    var interactor: ListPresenterInteractor?
    
    init(view: ListPresenterView, interactor: ListPresenterInteractor) {
        self.view = view
        self.interactor = interactor
        
        self.view?.presenter = self
        self.interactor?.presenter = self
    }

    func update(with array: [Friend]?) {
        self.updateCalled = true
        self.array = array ?? []
        self.view?.update()
    }
    
    func load() {
        self.loadCalled = true
        self.interactor?.load()
    }
    
    func select(at index: Int, with data: ImageTransferData) {
        self.selectCalled = true
    }
    
    var count: Int {
        return array.count
    }
    
    func setup(cell: ListTableCellProtocol, for index: Int) {
        self.setupCalled = true
    }
}

class ListTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testList() {
        let window = UIWindow()
        
        ListWireframe.display(in: window)
        
        XCTAssertNotNil(window.rootViewController)
        XCTAssert(window.rootViewController is UINavigationController)
        
        let navigation = window.rootViewController as! UINavigationController
        
        XCTAssertNotNil(navigation.viewControllers.first)
        XCTAssert(navigation.viewControllers.first is ListViewController)
    }
    
    func testListWireframeGenericType() {
        let window = UIWindow()
        
        ListWireframe.display(in: window, with: TestService.self)
        
        XCTAssertNotNil(window.rootViewController)
        XCTAssert(window.rootViewController is UINavigationController)
        
        let navigation = window.rootViewController as! UINavigationController
        
        XCTAssertNotNil(navigation.viewControllers.first)
        XCTAssert(navigation.viewControllers.first is ListViewController)
    }
    
    func testListPresenterInitCount() {
        let window = UIWindow()
        
        ListWireframe.display(in: window, with: TestService.self)
        
        XCTAssertNotNil(window.rootViewController)
        XCTAssert(window.rootViewController is UINavigationController)
        
        let navigation = window.rootViewController as! UINavigationController
        
        XCTAssertNotNil(navigation.viewControllers.first)
        XCTAssert(navigation.viewControllers.first is ListViewController)
        
        let view = navigation.viewControllers.first as! ListViewController
        
        XCTAssertEqual(view.presenter?.count, 0)
    }
    
    func testListLoad() {
        
        let view = ListViewController()
        let interactor = ListInteractor(type: TestService.self)
        let presenter = TestListPresenter(view: view, interactor: interactor)
        
        view.viewDidAppear(true)
        
        XCTAssertTrue(presenter.loadCalled)
        XCTAssertTrue(presenter.updateCalled)
        XCTAssertEqual(presenter.count, 1)
        
        XCTAssertFalse(presenter.setupCalled)
    }
    
    func testListSetup() {
        
        let view = ListViewController()
        let interactor = ListInteractor(type: TestService.self)
        let presenter = TestListPresenter(view: view, interactor: interactor)
        
        _ = view.view
        view.viewDidAppear(true)
        
        XCTAssertTrue(presenter.loadCalled)
        XCTAssertTrue(presenter.updateCalled)
        XCTAssertEqual(presenter.count, 1)
        
        XCTAssertFalse(presenter.setupCalled)
        
        let indexPath = IndexPath(row: 0, section: 0)
        guard let cell = view.tableView.cellForRow(at: indexPath) as? ListTableViewCell else {
            XCTFail()
            return
        }
        
        view.tableView(view.tableView, willDisplay: cell, forRowAt: indexPath)
        
        XCTAssertTrue(presenter.setupCalled)
    }
    
}
