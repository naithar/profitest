//
//  PhotoTest.swift
//  profiTest
//
//  Created by Sergey Minakov on 07.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import XCTest

@testable import profiTest

class PhotoTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testPhoto() {
        let window = UIWindow(frame: CGRect(x: 0, y: 0, width: 100, height: 200))
        
        PhotoWireframe.display(on: window, data: (nil, .zero), url: "url")
        
        XCTAssert(window.subviews.count > 0)
        XCTAssert(window.subviews.first is PhotoView)
    }
    
}
