//
//  ImageDownloaderTest.swift
//  profiTest
//
//  Created by Sergey Minakov on 07.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import XCTest

@testable import profiTest

class ImageDownloaderTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func testImageLoadAndCache() {
        let url = "http://tinyurl.com/jppypaz"
        
        let key = url.key
        XCTAssertNil(ImageCache.shared[key])
        let expectation = self.expectation(description: "image")
        
        ImageDownloader.shared.load(url: url) { image in
            XCTAssertNotNil(image)
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 10) { error in
            XCTAssertNil(error)
            XCTAssertNotNil(ImageCache.shared[key])
        }
    }
}
