//
//  AppTest.swift
//  profiTest
//
//  Created by Sergey Minakov on 07.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import XCTest

@testable import profiTest

class AppTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testApp() {
        var window: UIWindow?
        
        AppWireframe.start(with: &window)
        
        XCTAssertNotNil(window)
        XCTAssert(window is AppWindow)
        XCTAssertNotNil(window!.rootViewController)
        XCTAssert(window!.rootViewController is LoadingViewController)
    }
    
}
