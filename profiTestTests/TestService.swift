//
//  TestService.swift
//  profiTest
//
//  Created by Sergey Minakov on 07.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import XCTest
@testable import profiTest

final class TestService: FriendsServiceProtocol {
    
    static let shared = TestService()
    
    private(set) var authorized = false
    
    func checkState(callback: @escaping (FriendsServiceState) -> ()) {
        callback(self.authorized ? .authorized : .unknown)
    }
    
    func authorize(callback: @escaping (Bool) -> ()) {
        self.authorized = true
        callback(true)
    }
    
    func requestList(callback: @escaping ([Friend]?) -> ()) {
        let item = Friend(id: 1, firstName: "First", lastName: "Last")
        
        callback([item])
    }
    
}
