//
//  AuthTest.swift
//  profiTest
//
//  Created by Sergey Minakov on 07.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import XCTest

@testable import profiTest

class TestAuthPresenter: AuthInteractorPresenter, AuthViewPresenter {
    
    var listCalled = false
    var startCalled = false
    var authCalled = false
    
    var view: AuthPresenterView?
    var interactor: AuthPresenterInteractor?
    
    init(view: AuthPresenterView, interactor: AuthPresenterInteractor) {
        self.view = view
        self.interactor = interactor
        
        self.view?.presenter = self
        self.interactor?.presenter = self
    }
    
    func showList() {
        self.listCalled = true
    }
    
    func start() {
        self.startCalled = true
    }
    
    func auth() {
        self.authCalled = true
        self.interactor?.auth()
    }
}

class AuthTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testAuth() {
        let window = UIWindow()
        
        AuthWireframe.display(in: window)
        
        XCTAssertNotNil(window.rootViewController)
        XCTAssert(window.rootViewController is AuthViewController)
    }
    
    func testAuthWireframeGenericType() {
        let window = UIWindow()
        
        AuthWireframe.display(in: window)
        
        XCTAssertNotNil(window.rootViewController)
        XCTAssert(window.rootViewController is AuthViewController)
    }
    
    func testAuthStart() {
        let view = AuthViewController()
        let interactor = AuthInteractor(type: TestService.self)
        let presenter = TestAuthPresenter(view: view, interactor: interactor)
        
        view.viewDidAppear(true)
        
        XCTAssertTrue(presenter.startCalled)
        XCTAssertFalse(presenter.authCalled)
        XCTAssertFalse(presenter.listCalled)
    }
    
    func testAuthAuthToList() {
        let view = AuthViewController()
        let interactor = AuthInteractor(type: TestService.self)
        let presenter = TestAuthPresenter(view: view, interactor: interactor)
        
        view.viewDidAppear(true)
        
        XCTAssertTrue(presenter.startCalled)
        XCTAssertFalse(presenter.authCalled)
        XCTAssertFalse(presenter.listCalled)
        
        presenter.auth()
        
        XCTAssertTrue(TestService.shared.authorized)
        
        XCTAssertTrue(presenter.startCalled)
        XCTAssertTrue(presenter.authCalled)
        XCTAssertTrue(presenter.listCalled)
    }
}
