//
//  AppWireframe.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

struct AppWireframe {

    static func start(with window: inout UIWindow?) {
        let frame = UIScreen.main.bounds
        let appWindow = AppWindow(frame: frame)
        LoadingWireframe.display(in: appWindow)
        appWindow.makeKeyAndVisible()
        
        window = appWindow
    }
}
