//
//  FriendsServiceProtocol.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

protocol FriendsServiceProtocol {
    
    static var shared: Self { get }
    
    func checkState(callback: @escaping (FriendsServiceState) -> ())
    func authorize(callback: @escaping (Bool) -> ())
    func requestList(callback: @escaping ([Friend]?) -> ())
}

enum FriendsServiceState {
    case unknown
    case authorized
}
