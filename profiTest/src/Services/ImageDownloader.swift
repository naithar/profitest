//
//  ImageDownloader.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

class ImageDownloader {
    
    static let shared = ImageDownloader()
    
    private lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        let session = URLSession(configuration: configuration)
        
        return session
    }()
    
    final func load(url: String?, callback: @escaping (UIImage?) -> ()) {
        guard let urlString = url else {
            callback(nil)
            return
        }
        
        guard ImageCache.shared[urlString] == nil else {
            callback(ImageCache.shared[urlString])
            return
        }
        
        guard let url = URL(string: urlString) else {
            callback(nil)
            return
        }
        
        let task = self.session.dataTask(with: url) { data, _, error in
            guard error == nil,
                let data = data,
                let image = UIImage(data: data) else {
                    callback(nil)
                    return
            }
            
            ImageCache.shared[urlString] = image
            callback(image)
        }
        
        task.resume()
    }
}
