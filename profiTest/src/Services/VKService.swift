//
//  VKService.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit
import VKSdkFramework

final class VKService: NSObject {
    
    static let shared = VKService()
    
    private static let vkID = "5709475"
    
    fileprivate var authorizationCallback: ((Bool) -> ())?
    
    override init() {
        super.init()
        let instance = VKSdk.initialize(withAppId: type(of: self).vkID)
        instance?.uiDelegate = self
        instance?.register(self)
    }
    
}

extension VKService: FriendsServiceProtocol {
    
    final func checkState(callback: @escaping (FriendsServiceState) -> ()) {
        let scope = ["friends"]
        VKSdk.wakeUpSession(scope) { state, error in
            switch state {
            case .authorized:
                callback(.authorized)
            default:
                callback(.unknown)
            }
        }
    }
    
    final func authorize(callback: @escaping (Bool) -> ()) {
        self.authorizationCallback = callback
        let scope = ["friends"]
        VKSdk.authorize(scope)
    }
    
    final func requestList(callback: @escaping ([Friend]?) -> ()) {
        VKApi.friends()
            .get([
                "order" : "name",
                "fields" : "photo_100,photo_max_orig"
            ]).execute(resultBlock: { response in
                
                guard let json = response?.json as? [String : Any],
                    let items = json["items"] as? [Any] else {
                        callback(nil)
                        return
                }
                
                let friends = items.flatMap { json in
                    return Friend(json: json)
                }
                
                return callback(friends)
            }, errorBlock: { error in
                callback(nil)
            })
    }
}

extension VKService: VKSdkDelegate {
    
    public func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
    }
    
    public func vkSdkUserAuthorizationFailed() {
    }
    
    public func vkSdkAuthorizationStateUpdated(with result: VKAuthorizationResult!) {
        self.authorizationCallback?(result.state == .authorized)
        self.authorizationCallback = nil
    }
}

extension VKService: VKSdkUIDelegate {
    
    public func vkSdkShouldPresent(_ controller: UIViewController!) {
        guard let viewController = UIApplication.shared.delegate?.window??.rootViewController else {
            return
        }
        
        viewController.present(controller, animated: true, completion: nil)
    }
    
    public func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        
    }
}
