//
//  ImageCache.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

extension String {

    var key: String {
        return self.replacingOccurrences(of: ":", with: "_")
            .replacingOccurrences(of: "/", with: "_")
    }
}

class ImageCache {
    
    static let shared = ImageCache()
    
    private static var maxCount = 200
    
    private var cache = NSCache<NSString, UIImage>()
    
    init() {
        self.cache.countLimit = type(of: self).maxCount
        
        if let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let folder = documents.appendingPathComponent("image_cache", isDirectory: true)
            do {
                try FileManager.default.createDirectory(at: folder, withIntermediateDirectories: false, attributes: [:])
            } catch {
                //folder already exists
            }
        }
    }
    
    final func clear() {
        self.clearNSCache()
        self.clearStorage()
    }
    
    private func clearNSCache() {
        self.cache.removeAllObjects()
    }
    
    private func clearStorage() {
        guard let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return
        }
        
        let folder = documents.appendingPathComponent("image_cache", isDirectory: true)
        do {
            try FileManager.default.removeItem(at: folder)
            try FileManager.default.createDirectory(at: folder, withIntermediateDirectories: false, attributes: [:])
        } catch {
            NSLog("Error clearing cache: \(error)")
        }
    }

    subscript(key: String) -> UIImage? {
        get {
            return self.fromNSCache(using: key.key) ?? self.fromStorage(using: key.key)
        }
        set {
            self.store(newValue, using: key.key)
        }
    }
    
    private func fromNSCache(using key: String) -> UIImage? {
        let key = NSString(string: key)
        return self.cache.object(forKey: key)
    }
    
    private func fromStorage(using key: String) -> UIImage? {
        guard let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        
        let folder = documents.appendingPathComponent("image_cache", isDirectory: true)
        let fileURL = folder.appendingPathComponent(key)
        
        let image = UIImage(contentsOfFile: fileURL.path)
        
        self.storeInNSCache(image: image, key: key)
        
        return image
    }
    
    private func store(_ image: UIImage?, using key: String) {
        self.storeInNSCache(image: image, key: key)
        self.storeInStorage(image: image, key: key)
    }
    
    private func storeInNSCache(image: UIImage?, key: String) {
        guard let image = image else {
            return
        }
        
        let key = NSString(string: key)
        self.cache.setObject(image, forKey: key)
    }
    
    private func storeInStorage(image: UIImage?, key: String) {
        guard let image = image,
            let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
            let imageData = UIImagePNGRepresentation(image) else {
                return
        }
        
        let folder = documents.appendingPathComponent("image_cache", isDirectory: true)
        let fileURL = folder.appendingPathComponent(key)
        
        guard !FileManager.default.fileExists(atPath: fileURL.path) else {
            //already cached
            return
        }
        
        do {
            try imageData.write(to: fileURL, options: .atomic)
        } catch {
            NSLog("Error in storing image in cache: \(error)")
        }
        
    }
}
