//
//  LoadingRouter.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

struct LoadingRouter {

    func auth() {
        guard let window = UIApplication.shared.delegate?.window else {
            return
        }
        
        AuthWireframe.display(in: window)
    }
    
    func list() {
        guard let window = UIApplication.shared.delegate?.window else {
            return
        }
        
        ListWireframe.display(in: window)
    }
}
