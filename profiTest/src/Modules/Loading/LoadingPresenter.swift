//
//  LoadingPresenter.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

protocol LoadingPreseterView: class {
    
    var presenter: LoadingViewPresenter? { get set }
}

protocol LoadingPresenterInteractor {
    
    weak var presenter: LoadingInteractorPresenter? { get set }
    
    func checkAuthorization()
}

class LoadingPresenter {

    weak var view: LoadingPreseterView?
    var interactor: LoadingPresenterInteractor?
    
    let router = LoadingRouter()
    
    init(view: LoadingPreseterView, interactor: LoadingPresenterInteractor) {
        self.view = view
        self.interactor = interactor
        
        self.view?.presenter = self
        self.interactor?.presenter = self
    }
}

extension LoadingPresenter: LoadingViewPresenter {
    
    final func start() {
        self.interactor?.checkAuthorization()
    }
}

extension LoadingPresenter: LoadingInteractorPresenter {
    
    final func showList() {
        if Thread.isMainThread {
            self.router.list()
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.router.list()
            }
        }
    }
    
    final func showAuth() {
        if Thread.isMainThread {
            self.router.auth()
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.router.auth()
            }
        }
    }
}
