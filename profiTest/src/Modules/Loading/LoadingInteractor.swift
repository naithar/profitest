//
//  LoadingInteractor.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import Foundation

protocol LoadingInteractorPresenter: class {
    
    func showList()
    func showAuth()
}

class LoadingInteractor<T: FriendsServiceProtocol> {
    
    weak var presenter: LoadingInteractorPresenter?
    
    fileprivate let service: T
    
    init(type: T.Type) {
        self.service = T.shared
    }
}

extension LoadingInteractor: LoadingPresenterInteractor {
    
    final func checkAuthorization() {
        self.service.checkState { [weak self] state in
            switch state {
            case .authorized:
                self?.presenter?.showList()
            default:
                self?.presenter?.showAuth()
            }
        }
    }
}
