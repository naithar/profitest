//
//  LoadingViewController.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

protocol LoadingViewPresenter {
    
    func start()
}

class LoadingViewController: UIViewController {

    var presenter: LoadingViewPresenter?
    
    private var activity = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.presenter?.start()
    }
    
    private func setupView() {
        self.view.backgroundColor = .white
        
        self.activity.translatesAutoresizingMaskIntoConstraints = false
        self.activity.startAnimating()
        self.view.addSubview(self.activity)
        
        let centerX = NSLayoutConstraint(item: self.activity,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: self.view,
                                         attribute: .centerX,
                                         multiplier: 1.0, constant: 0)
        
        let centerY = NSLayoutConstraint(item: self.activity,
                                         attribute: .centerY,
                                         relatedBy: .equal,
                                         toItem: self.view,
                                         attribute: .centerY,
                                         multiplier: 1.0, constant: 0)
        
        self.view.addConstraints([centerX, centerY])
    }
}

extension LoadingViewController: LoadingPreseterView {
    
}
