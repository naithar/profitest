//
//  AuthViewController.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

protocol AuthViewPresenter {
    
    func start()
    func auth()
}

class AuthViewController: UIViewController {

    var presenter: AuthViewPresenter?
    
    lazy var onceStart: () = { [weak self] in
        self?.presenter?.start()
    }()
    
    private var button = UIButton(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        _ = self.onceStart
    }
    
    private func setupView() {
        self.view.backgroundColor = .white
        
        self.setupButton()
        self.setupConstraints()
    }
    
    private func setupButton() {
        self.button.setTitle("Войти используя ВКонтакте", for: .normal)
        self.button.setTitleColor(.white, for: .normal)
        self.button.setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .highlighted)
        self.button.layer.backgroundColor = UIColor(
            red: 80 / 255.0,
            green: 114 / 255.0,
            blue: 153 / 255.0, alpha: 1).cgColor
        
        self.button.layer.cornerRadius = 7
        self.button.layer.shadowRadius = 5
        self.button.layer.shadowColor = UIColor.black.cgColor
        self.button.layer.shadowOffset = CGSize(width: 5, height: 5)
        self.button.layer.shadowOpacity = 0.75
        self.button.layer.masksToBounds = false
        
        let selector = #selector(type(of: self).buttonAction(_:))
        self.button.addTarget(self, action: selector, for: .touchUpInside)
        
        self.button.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.button)
    }
    
    private func setupConstraints() {
        let centerX = NSLayoutConstraint(item: self.button,
                                         attribute: .centerX,
                                         relatedBy: .equal,
                                         toItem: self.view,
                                         attribute: .centerX,
                                         multiplier: 1.0, constant: 0)
        
        let centerY = NSLayoutConstraint(item: self.button,
                                         attribute: .centerY,
                                         relatedBy: .equal,
                                         toItem: self.view,
                                         attribute: .centerY,
                                         multiplier: 1.0, constant: 0)
        
        let width = NSLayoutConstraint(item: self.button,
                                       attribute: .width,
                                       relatedBy: .equal,
                                       toItem: self.button,
                                       attribute: .width,
                                       multiplier: 0, constant: 250)
        
        let height = NSLayoutConstraint(item: self.button,
                                        attribute: .height,
                                        relatedBy: .equal,
                                        toItem: self.button,
                                        attribute: .height,
                                        multiplier: 0, constant: 50)
        
        self.view.addConstraints([centerX, centerY, width, height])
    }
}

extension AuthViewController {
    
    final func buttonAction(_ button: UIButton) {
        self.presenter?.auth()
    }
}

extension AuthViewController: AuthPresenterView {
    
}
