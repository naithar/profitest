//
//  AuthRouter.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

struct AuthRouter {
    
    func list() {
        guard let window = UIApplication.shared.delegate?.window else {
            return
        }
        
        ListWireframe.display(in: window)
    }
}
