//
//  AuthInteractor.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import Foundation

protocol AuthInteractorPresenter: class {
    
    func showList()
}

class AuthInteractor<T: FriendsServiceProtocol> {
    
    weak var presenter: AuthInteractorPresenter?
    
    fileprivate let service: T
    
    init(type: T.Type) {
        self.service = T.shared
    }
}

extension AuthInteractor: AuthPresenterInteractor {
    
    final func checkAuthorization() {
        self.service.checkState { [weak self] state in
            switch state {
            case .authorized:
                self?.presenter?.showList()
            default:
                break
            }
        }
    }
    
    final func auth() {
        self.service.authorize { [weak self] success in
            guard success else {
                return
            }
            
            self?.checkAuthorization()
        }
    }
}
