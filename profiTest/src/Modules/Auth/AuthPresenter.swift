//
//  AuthPresenter.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

protocol AuthPresenterView: class {
    
    var presenter: AuthViewPresenter? { get set }
}

protocol AuthPresenterInteractor {
    
    weak var presenter: AuthInteractorPresenter? { get set }
    
    func checkAuthorization()
    
    func auth()
}

class AuthPresenter {
    
    weak var view: AuthPresenterView?
    var interactor: AuthPresenterInteractor?
    
    let router = AuthRouter()
    
    init(view: AuthPresenterView?, interactor: AuthPresenterInteractor?) {
        self.view = view
        self.interactor = interactor
        
        self.view?.presenter = self
        self.interactor?.presenter = self
    }
}

extension AuthPresenter: AuthViewPresenter {
    
    final func start() {
        self.interactor?.checkAuthorization()
    }
    
    final func auth() {
        self.interactor?.auth()
    }
}

extension AuthPresenter: AuthInteractorPresenter {
    
    final func showList() {
        if Thread.isMainThread {
            self.router.list()
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.router.list()
            }
        }
    }
}
