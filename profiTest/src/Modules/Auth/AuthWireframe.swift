//
//  AuthWireframe.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

struct AuthWireframe {
    
    static func display(in window: UIWindow?) {
        self.display(in: window, with: VKService.self)
    }
    
    static func display<T: FriendsServiceProtocol>(in window: UIWindow?, with serviceType: T.Type) {
        guard let window = window else {
            return
        }
        
        let view = AuthViewController()
        let interactor = AuthInteractor(type: serviceType)
        _ = AuthPresenter(view: view, interactor: interactor)
        
        window.rootViewController = view
    }
}
