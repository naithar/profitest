//
//  PhotoWireframe.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

struct PhotoWireframe {
    
    static let tag = 10050
    
    static func display(on window: UIWindow?, data: ImageTransferData, url: String) {
        guard let window = window else {
            return
        }
        
        let view = PhotoView(data: data)
        view.tag = PhotoWireframe.tag
        view.frame = window.bounds
        let interactor = PhotoInteractor()
        _ = PhotoPresenter(url: url, view: view, interactor: interactor)
        
        window.addSubview(view)
    }
}
