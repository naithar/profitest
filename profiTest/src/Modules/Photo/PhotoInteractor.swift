//
//  PhotoInteractor.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import Foundation
import UIKit

protocol PhotoInteractorPresenter: class {
    
}

class PhotoInteractor {
    
    weak var presenter: PhotoInteractorPresenter?
}

extension PhotoInteractor: PhotoPresenterInteractor {
    
    func loadImage(url: String, callback: @escaping (UIImage?) -> ()) {
        ImageDownloader.shared.load(url: url, callback: callback)
    }
}
