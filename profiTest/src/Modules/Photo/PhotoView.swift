//
//  PhotoView.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

protocol PhotoViewPresenter {
    
    func close()
    
    func start()
}

class PhotoView: UIView {

    var presenter: PhotoViewPresenter?
    
    fileprivate var image = UIImageView()
    
    fileprivate var button = UIButton(type: .custom)
    
    private var transferData: ImageTransferData?
    
    convenience init(data: ImageTransferData) {
        self.init(frame: .zero)
        
        self.transferData = data
        self.commonInit(with: data)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func commonInit(with data: ImageTransferData) {
        self.setupImage(with: data)
        self.setupButton()
        self.setupTapGesture()
        
        self.setupConstraints()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        self.startAnimation()
        
        self.presenter?.start()
    }
    
    private func setupImage(with data: ImageTransferData) {
        self.image.image = data.0
        self.image.frame = data.1
        self.image.contentMode = .scaleAspectFit
        self.image.layer.cornerRadius = 7
        self.image.clipsToBounds = true
        
        self.addSubview(self.image)
    }
    
    private func setupButton() {
        self.button.setTitle("Закрыть", for: .normal)
        self.button.setTitleColor(.white, for: .normal)
        self.button.setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .highlighted)
        
        let selector = #selector(type(of: self).buttonAction(_:))
        self.button.addTarget(self, action: selector, for: .touchUpInside)
        
        self.button.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.button)
    }
    
    private func setupTapGesture() {
        let selector = #selector(type(of: self).tapAction(_:))
        let recognizer = UITapGestureRecognizer(target: self, action: selector)
        self.addGestureRecognizer(recognizer)
    }
    
    private func setupConstraints() {
        let buttonBottom = NSLayoutConstraint(item: self.button,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: self,
                                              attribute: .bottom,
                                              multiplier: 1.0, constant: -15)
        
        let buttonCenterX = NSLayoutConstraint(item: self.button,
                                               attribute: .centerX,
                                               relatedBy: .equal,
                                               toItem: self,
                                               attribute: .centerX,
                                               multiplier: 1.0, constant: 0)
        
        let buttonWidth = NSLayoutConstraint(item: self.button,
                                             attribute: .width,
                                             relatedBy: .equal,
                                             toItem: self.button,
                                             attribute: .width,
                                             multiplier: 0, constant: 250)
        
        let buttonHeight = NSLayoutConstraint(item: self.button,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: self.button,
                                              attribute: .height,
                                              multiplier: 0, constant: 60)
        
        self.addConstraints([buttonBottom, buttonCenterX, buttonWidth, buttonHeight])
    }
    
    private func startAnimation() {
        self.backgroundColor = .clear
        
        UIView.animate(withDuration: 0.5, animations: {
            self.image.frame = self.bounds
            self.layer.backgroundColor = UIColor.black.withAlphaComponent(0.5).cgColor
        }, completion: { _ in
            
        })
    }
    
    final func hide() {
        guard let data = self.transferData else {
            UIView.animate(
                withDuration: 0.3,
                animations: {
                    self.alpha = 0
            }, completion: { _ in
                self.removeFromSuperview()
            })
            return
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.image.frame = data.1
            self.image.image = data.0 ?? self.image.image
            
            self.layer.backgroundColor = UIColor.clear.cgColor
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }
}

extension PhotoView {
 
    final func buttonAction(_ button: UIButton) {
        self.presenter?.close()
    }
    
    final func tapAction(_ recognizer: UITapGestureRecognizer) {
        if self.button.isHidden {
            self.button.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.button.alpha = 1
            }
        } else {
            UIView.animate(
                withDuration: 0.3,
                animations: {
                     self.button.alpha = 0
            }, completion: { _ in
                self.button.isHidden = true
            })
        }
    }
}

extension PhotoView: PhotoPresenterView {
    
    final func display(image: UIImage) {
        self.image.image = image
    }
}
