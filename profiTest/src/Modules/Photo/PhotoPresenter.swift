//
//  PhotoPresenter.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

protocol PhotoPresenterView: class {
    
    var presenter: PhotoViewPresenter? { get set }
    
    func display(image: UIImage)
}

protocol PhotoPresenterInteractor {
 
    var presenter: PhotoInteractorPresenter? { get set }
    
    func loadImage(url: String, callback: @escaping (UIImage?) -> ())
}

class PhotoPresenter {
    
    weak var view: PhotoPresenterView?
    var interactor: PhotoPresenterInteractor?
    
    let router = PhotoRouter()
    
    fileprivate var url: String?
    
    init(url: String?, view: PhotoPresenterView, interactor: PhotoPresenterInteractor) {
        self.url = url
        self.view = view
        self.interactor = interactor
        
        self.view?.presenter = self
        self.interactor?.presenter = self
    }
}

extension PhotoPresenter: PhotoViewPresenter {
    
    final func close() {
        self.router.close()
    }
    
    final func start() {
        guard let url = self.url else {
            return
        }
        
        self.interactor?.loadImage(url: url) { [weak self] image in
            guard let image = image else {
                return
            }
            
            self?.view?.display(image: image)
        }
    }
}

extension PhotoPresenter: PhotoInteractorPresenter {
    
}
