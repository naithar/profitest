//
//  PhotoRouter.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

class PhotoRouter {
    
    func close() {
        guard let window = UIApplication.shared.delegate?.window,
            let view = window?.viewWithTag(PhotoWireframe.tag) as? PhotoView else {
                return
        }
        
        view.hide()
    }
}
