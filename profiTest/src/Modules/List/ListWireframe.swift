//
//  ListWireframe.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

struct ListWireframe {
    
    static func display(in window: UIWindow?) {
        self.display(in: window, with: VKService.self)
    }
    
    static func display<T: FriendsServiceProtocol>(in window: UIWindow?, with serviceType: T.Type) {
        guard let window = window else {
            return
        }
        
        let view = ListViewController()
        let interactor = ListInteractor(type: serviceType)
        _ = ListPresenter(view: view, interactor: interactor)
        let navigation = UINavigationController(rootViewController: view)
        
        window.rootViewController = navigation
    }
}
