//
//  ListPresenter.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

protocol ListPresenterView: class {
    
    var presenter: ListViewPresenter? { get set }
    
    func update()
}

protocol ListPresenterInteractor {
   
    weak var presenter: ListInteractorPreseter? { get set }
    
    func load()
    
    func loadImage(at url: String, callback: @escaping (UIImage?) -> ())
}

typealias ImageTransferData = (UIImage?, CGRect)

class ListPresenter {
    
    weak var view: ListPresenterView?
    var interactor: ListPresenterInteractor?
    
    let router = ListRouter()
    var data = [Friend]()
    
    init(view: ListPresenterView, interactor: ListPresenterInteractor) {
        self.view = view
        self.interactor = interactor
        
        self.view?.presenter = self
        self.interactor?.presenter = self
    }
}

extension ListPresenter: ListViewPresenter {
    
    final func load() {
        self.interactor?.load()
    }
    
    final var count: Int {
        return self.data.count
    }
    
    final func friend(at index: Int) -> Friend? {
        guard self.data.count > index else {
            return nil
        }
        
        return self.data[index]
    }
    
    final func select(at index: Int, with data: ImageTransferData) {
        guard let url = self.friend(at: index)?.photoOriginal else {
            return
        }
        
        self.router.photo(with: url, data: data)
    }
    
    final func setup(cell: ListTableCellProtocol, for index: Int) {
        guard let data = self.friend(at: index) else {
            return
        }
        
        cell.start()
        let timestamp = cell.timestamp
        cell.set(name: data.name)
        
        guard let photo = data.photoOriginal else {
            return
        }
        
        self.interactor?.loadImage(at: photo) { image in
            guard timestamp == cell.timestamp else {
                return
            }
            
            if Thread.isMainThread {
                cell.set(image: image)
            } else {
                DispatchQueue.main.sync {
                    cell.set(image: image)
                }
            }
        }
    }
}

extension ListPresenter: ListInteractorPreseter {
    
    final func update(with array: [Friend]?) {
        guard let array = array else {
            if Thread.isMainThread {
                self.view?.update()
            } else {
                DispatchQueue.main.async { [weak self] in
                    self?.view?.update()
                }
            }
            return
        }
        
        self.data = array
        
        if Thread.isMainThread {
            self.view?.update()
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.view?.update()
            }
        }
    }
}
