//
//  ListRouter.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

struct ListRouter {
    
    func photo(with url: String, data: ImageTransferData) {
        guard let window = UIApplication.shared.delegate?.window else {
            return
        }
        
        PhotoWireframe.display(on: window, data: data, url: url)
    }
}
