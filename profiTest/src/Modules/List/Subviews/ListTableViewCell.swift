//
//  ListTableViewCell.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

protocol ListTableCellProtocol {
    
    var timestamp: TimeInterval { get }
    
    func start()
    func set(name: String)
    func set(image: UIImage?)
}

class ListTableViewCell: UITableViewCell {
    
    private(set) var avatar = UIImageView()
    private(set) var name = UILabel()
    
    fileprivate(set) var timestamp: TimeInterval = 0
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.commonInit()
    }
    
    private func commonInit() {
        self.separatorInset = .zero
        
        self.setupView()
        self.setupConstraints()
    }
    
    private func setupView() {
        self.setupAvatar()
        self.setupName()
    }
    
    private func setupAvatar() {
        self.avatar.contentMode = .scaleAspectFill
        self.avatar.layer.cornerRadius = 7
        self.avatar.clipsToBounds = true
        self.avatar.backgroundColor = UIColor.gray.withAlphaComponent(0.25)
        
        self.avatar.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(self.avatar)
    }
    
    private func setupName() {
        self.name.font = UIFont.systemFont(ofSize: 16)
        self.name.textColor = .black
        
        self.name.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(self.name)
    }
    
    private func setupConstraints() {
        self.setupAvatarConstraints()
        self.setupNameConstraints()
    }
    
    private func setupAvatarConstraints() {
        let avatarCenterY = NSLayoutConstraint(item: self.avatar,
                                               attribute: .centerY,
                                               relatedBy: .equal,
                                               toItem: self.contentView,
                                               attribute: .centerY,
                                               multiplier: 1.0, constant: 0)
        
        let avatarLeft = NSLayoutConstraint(item: self.avatar,
                                            attribute: .left,
                                            relatedBy: .equal,
                                            toItem: self.contentView,
                                            attribute: .left,
                                            multiplier: 1.0, constant: 15)
        
        let avatarWidth = NSLayoutConstraint(item: self.avatar,
                                             attribute: .width,
                                             relatedBy: .equal,
                                             toItem: self.avatar,
                                             attribute: .width,
                                             multiplier: 0, constant: 50)
        
        let avatarHeight = NSLayoutConstraint(item: self.avatar,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: self.avatar,
                                              attribute: .height,
                                              multiplier: 0, constant: 50)
        
        self.addConstraints([avatarCenterY, avatarLeft, avatarWidth, avatarHeight])
    }
    
    private func setupNameConstraints() {
        let nameCenterY = NSLayoutConstraint(item: self.name,
                                             attribute: .centerY,
                                             relatedBy: .equal,
                                             toItem: self.contentView,
                                             attribute: .centerY,
                                             multiplier: 1.0, constant: 0)
        
        let nameLeft = NSLayoutConstraint(item: self.name,
                                          attribute: .left,
                                          relatedBy: .equal,
                                          toItem: self.avatar,
                                          attribute: .right,
                                          multiplier: 1.0, constant: 10)
        
        let nameRight = NSLayoutConstraint(item: self.name,
                                           attribute: .right,
                                           relatedBy: .equal,
                                           toItem: self.contentView,
                                           attribute: .right,
                                           multiplier: 1.0, constant: -10)
        nameRight.priority = 900
        
        self.addConstraints([nameCenterY, nameLeft, nameRight])
    }
    
    private func reset() {
        self.avatar.image = nil
        self.name.text = nil
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.reset()
    }
}

extension ListTableViewCell: ListTableCellProtocol {
    
    final func start() {
        self.timestamp = Date().timeIntervalSince1970
    }
    
    final func set(name: String) {
        self.name.text = name
    }
    
    final func set(image: UIImage?) {
        self.avatar.image = image
    }
}
