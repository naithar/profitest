//
//  ListViewController.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

protocol ListViewPresenter {
    
    func load()
    
    func select(at index: Int, with data: ImageTransferData)
    
    var count: Int { get }
    
    func setup(cell: ListTableCellProtocol, for index: Int)
}

class ListViewController: UIViewController {

    var presenter: ListViewPresenter?
    
    private(set) var tableView = UITableView()
    fileprivate var refreshControl = UIRefreshControl()
    fileprivate var reloadButton = UIButton(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.presenter?.load()
    }
    
    private func setupView() {
        self.view.backgroundColor = .white
        self.title = "Список друзей"
        
        self.setupTableView()
        self.setupReloadButton()
        self.setupConstraints()
    }
    
    private func setupTableView() {
        self.tableView.register(ListTableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.separatorStyle = .singleLine
        self.tableView.rowHeight = 80
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.showActivity()
        
        let selector = #selector(type(of: self).refreshAction(_:))
        self.refreshControl.addTarget(self, action: selector, for: .valueChanged)
        self.tableView.addSubview(self.refreshControl)
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.tableView)
    }
    
    private func setupReloadButton() {
        self.reloadButton.setTitle("Повторить загрузку", for: .normal)
        self.reloadButton.setTitleColor(.black, for: .normal)
        self.reloadButton.setTitleColor(UIColor.black.withAlphaComponent(0.5), for: .highlighted)
        
        let selector = #selector(type(of: self).reloadButtonAction(_:))
        self.reloadButton.addTarget(self, action: selector, for: .touchUpInside)
        self.reloadButton.frame.size.height = 100
    }
    
    private func setupConstraints() {
        let top = NSLayoutConstraint(item: self.tableView,
                                     attribute: .top,
                                     relatedBy: .equal,
                                     toItem: self.view,
                                     attribute: .top,
                                     multiplier: 1.0, constant: 0)
        
        let left = NSLayoutConstraint(item: self.tableView,
                                      attribute: .left,
                                      relatedBy: .equal,
                                      toItem: self.view,
                                      attribute: .left,
                                      multiplier: 1.0, constant: 0)
        
        let right = NSLayoutConstraint(item: self.tableView,
                                       attribute: .right,
                                       relatedBy: .equal,
                                       toItem: self.view,
                                       attribute: .right,
                                       multiplier: 1.0, constant: 0)
        
        let bottom = NSLayoutConstraint(item: self.tableView,
                                        attribute: .bottom,
                                        relatedBy: .equal,
                                        toItem: self.view,
                                        attribute: .bottom,
                                        multiplier: 1.0, constant: 0)
        
        self.view.addConstraints([top, left, right, bottom])
    }
    
    fileprivate func showActivity() {
        let activity = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activity.frame.size.height = 100
        activity.startAnimating()
        self.tableView.tableFooterView = activity
    }
}

extension ListViewController {
    
    final func refreshAction(_ refresh: UIRefreshControl) {
        self.presenter?.load()
    }
    
    final func reloadButtonAction(_ button: UIButton) {
        self.showActivity()
        self.presenter?.load()
    }
}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? ListTableViewCell {
            self.presenter?.setup(cell: cell, for: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        guard let cell = tableView.cellForRow(at: indexPath) as? ListTableViewCell,
            let window = UIApplication.shared.delegate?.window else {
                return
        }
        
        let avatar = cell.avatar
        let image = avatar.image
        let rect = avatar.convert(avatar.bounds, to: window)
        
        self.presenter?.select(at: indexPath.row, with: (image, rect))
    }
}

extension ListViewController: ListPresenterView {
    
    final func update() {
        self.refreshControl.endRefreshing()
        self.tableView.reloadData()
        
        if self.presenter?.count == 0 {
            self.tableView.tableFooterView = self.reloadButton
        } else {
            self.tableView.tableFooterView = UIView()
        }
    }
}
