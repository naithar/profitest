//
//  ListInteractor.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import Foundation
import UIKit

protocol ListInteractorPreseter: class {
    
    func update(with array: [Friend]?)
}

class ListInteractor<T: FriendsServiceProtocol> {
    
    weak var presenter: ListInteractorPreseter?
    
    fileprivate let service: T
    
    init(type: T.Type) {
        self.service = T.shared
    }
}

extension ListInteractor: ListPresenterInteractor {
    
    final func load() {
        self.service.requestList { [weak self] array in
            self?.presenter?.update(with: array)
        }
    }
    
    final func loadImage(at url: String, callback: @escaping (UIImage?) -> ()) {
        ImageDownloader.shared.load(url: url, callback: callback)
    }
}
