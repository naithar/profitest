//
//  Friend.swift
//  profiTest
//
//  Created by Sergey Minakov on 06.11.16.
//  Copyright © 2016 naithar. All rights reserved.
//

import UIKit

struct Friend {
    
    private(set) var id: Int
    private(set) var firstName: String
    private(set) var lastName: String
    private(set) var photo: String?
    private(set) var photoOriginal: String?
    
    var name: String {
        return "\(self.firstName) \(self.lastName)"
    }
    
    init?(json: Any) {
        guard let json = json as? [String : Any] else {
            return nil
        }
        
        guard let idValue = json["id"] as? NSNumber,
            let firstName = json["first_name"] as? String,
            let lastName = json["last_name"] as? String else {
            return nil
        }
        
        let photo = json["photo_100"] as? String
        let photoOriginal = json["photo_max_orig"] as? String
        
        self.id = idValue.intValue
        self.firstName = firstName
        self.lastName = lastName
        self.photo = photo
        self.photoOriginal = photoOriginal
    }
    
    init(id: Int, firstName: String, lastName: String) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
    }
}
